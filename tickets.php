<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Ticket: <span class="color_dark_blue">Create site</span></h1>

                        <div class="tickets_heading">

                            <ul class="tickets___meta">
                                <li>
                                    <span>Messages</span>
                                    <strong>132</strong>
                                </li>
                                <li>
                                    <span>Sended</span>
                                    <strong>03/12/2019  8:05:45</strong>
                                </li>
                                <li>
                                    <span>Status</span>
                                    <strong>Answered</strong>
                                </li>
                            </ul>

                            <div class="tickets_heading__button">
                                <a href="#" class="btn btn_md">CLOSE ticket</a>
                            </div>

                        </div>

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>CREATE NEW MESSAGE</h4>
                                <span class="panel__close"><i class="fas fa-caret-up"></i></span>
                            </div>
                            <div class="panel__body">
                                <form class="form">
                                    <div class="form_group">
                                        <label class="form_label">Message</label>
                                        <textarea class="form_control" name="message" placeholder="" rows="6">Lydnikova 15</textarea>
                                     </div>
                                    <div class="btn_group">
                                        <button type="submit" class="btn btn_lg">Create ticket</button>
                                        <button type="reset" class="btn btn_border">CANCEL</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel__heading panel__heading_border">
                                <h4><span>MESSAGES HISTORY</span> <span class="panel__heading_value">2</span></h4>
                            </div>
                            <div class="panel__body pt_20">

                                <div class="post mb_25">
                                    <div class="post__meta">
                                        <div class="post__meta_author">Support</div>
                                        <div class="post__meta_date">10/12/2019 8:00:15</div>
                                    </div>
                                    <div class="post__content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec dapibus neque, luctus fringilla nisl. Proin sed est id diam placerat ultrices. In nulla elit, porta eget turpis non, fermentum aliquet ex. Nulla accumsan nunc ac tellus efficitur, in varius est fringilla. Vestibulum finibus vitae elit quis efficitur. Ut vitae urna tortor. Ut sit amet velit nec odio pellentesque commodo. Suspendisse viverra dignissim magna, et molestie odio vestibulum a. Fusce et ex ac arcu vehicula semper eu non enim. Integer vulputate a magna quis eleifend felis posuere tristique libero vitae, efficitur aliquet ante. Nulla tincidunt purus tortor. Integer sed tristique justo.
                                    </div>
                                </div>

                                <div class="post post_invert">
                                    <div class="post__meta">
                                        <div class="post__meta_author">Alex Cuprum</div>
                                        <div class="post__meta_date">10/12/2019 8:00:15</div>
                                    </div>
                                    <div class="post__content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec dapibus neque, luctus fringilla nisl. Proin sed est id diam placerat ultrices. In nulla elit, porta eget turpis non, fermentum aliquet ex. Nulla accumsan nunc ac tellus efficitur, in varius est fringilla. Vestibulum finibus vitae elit quis efficitur. Ut vitae urna tortor. Ut sit amet velit nec odio pellentesque commodo. Suspendisse viverra dignissim magna, et molestie odio vestibulum a. Fusce et ex ac arcu vehicula semper eu non enim. Integer vulputate a magna quis eleifend felis posuere tristique libero vitae, efficitur aliquet ante. Nulla tincidunt purus tortor. Integer sed tristique justo.
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
