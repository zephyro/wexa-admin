// SVG IE11 support
svg4everybody();

$(".btn_modal").fancybox({
    'padding'    : 0
});

// Nav

(function() {

    $('.sidebar_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.page').toggleClass('sidebar_open');
    });

    $('.dropdown > a').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.dropdown').toggleClass('open');
        $(this).closest('.dropdown').find('ul').slideToggle('fast');
    });


}());


$('.notify__label').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.notify').toggleClass('open');
});

// hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".notify").length === 0) {
        $(".notify").removeClass('open');
    }
});


$(".sidebar__wrap").mCustomScrollbar({
    theme:"minimal-dark",
    scrollInertia:700
});


var markets = new Swiper('.markets__slider', {
    slidesPerView: 'auto',
    spaceBetween: 15,
});

// Tabs

(function() {

    $('.tabs_nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());



// Accordion
(function() {

    $('.accordion__heading').on('click touchstart', function(e){
        e.preventDefault();

        var box = $(this).closest('li');
        box.toggleClass('open');
        box.find('.accordion__content').slideToggle('fast');
    });

}());



// Select
(function() {

    $('.select__active').on('click touchstart', function(e){
        e.preventDefault();

        if($(this).closest('.select').hasClass('open')){
            $(this).closest('.select').toggleClass('open');
        }
        else {
            $('.select').removeClass('open');
            $(this).closest('.select').toggleClass('open');
        }
    });

    $('.select__item').on('click touchstart', function(){
        var box = $(this).closest('.select');
        var data = $(this).find('.select__item_label').html();

        box.removeClass('open');
        box.find('.select__active').html(data);

    });

}());


// Packages

(function() {

    $('.package').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.packages');

        $(this).closest('.packages').find('.package').removeClass('active');
        $(this).addClass('active');

        box.find('.tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

var $s1 = $(".source_amount_one");

$s1.ionRangeSlider({
    skin: "round",
    hide_min_max: true
});

$s1.on("change", function () {
    var data = $(this).prop("value");

    data = '$' + data;

    $(this).closest('.source_tab').find('.source__data_value').text(data);
    console.log(data); // FROM value
});


var $s2 = $(".source_amount_two");

$s2.ionRangeSlider({
    skin: "round",
    hide_min_max: true
});

$s2.on("change", function () {
    var data = $(this).prop("value");

    data = '$' + data;

    $(this).closest('.source_tab').find('.source__data_value').text(data);
    console.log(data); // FROM value
});


var $s3 = $(".source_amount_three");

$s3.ionRangeSlider({
    skin: "round",
    hide_min_max: true
});

$s3.on("change", function () {
    var data = $(this).prop("value");

    data = '$' + data;

    $(this).closest('.source_tab').find('.source__data_value').text(data);
    console.log(data); // FROM value
});


// Packages
(function() {

    $('.package').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.page');

        $(this).closest('.packages').find('.package').removeClass('active');
        $(this).closest('.package').addClass('active');

        box.find('.source_tab').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

// Panel collapse
(function() {

    $('.panel__close').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.panel').toggleClass('collapse');
        $(this).closest('.panel').find('.panel__body').slideToggle('fast');
    });

}());


