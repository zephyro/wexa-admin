<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Payouts</h1>

                        <!-- Widget -->
                        <?php include('inc/widget.inc.php') ?>
                        <!-- -->

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>REPLENISH WALLET</h4>
                            </div>
                            <div class="panel__body">
                                <form class="form">

                                    <div class="row">
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">select cryptocurrency</label>
                                                <div class="select">
                                                    <div class="select__active">
                                                        <i>
                                                            <img src="img/icon__bitcoin.png" class="img-fluid" alt="">
                                                        </i>
                                                        <span>Bitcoin</span>
                                                    </div>
                                                    <i class="fas fa-caret-down"></i>

                                                    <div class="select__dropdown">
                                                        <label class="select__item">
                                                            <input type="radio" name="pay" checked>
                                                            <div class="select__item_label">
                                                                <i>
                                                                    <img src="img/icon__bitcoin.png" class="img-fluid" alt="">
                                                                </i>
                                                                <span>Bitcoin</span>
                                                            </div>
                                                        </label>
                                                        <label class="select__item">
                                                            <input type="radio" name="pay">
                                                            <div class="select__item_label">
                                                                <i>
                                                                    <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                                                                        <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                    </svg>
                                                                </i>
                                                                <span>USD</span>
                                                            </div>
                                                        </label>
                                                        <label class="select__item">
                                                            <input type="radio" name="pay">
                                                            <div class="select__item_label">
                                                                <i>
                                                                    <img src="img/icon__bitcoin.png" class="img-fluid" alt="">
                                                                </i>
                                                                <span>LiteCoin</span>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">ENTER AMOUNT, USD</label>
                                                <input class="form_control" type="text" name="name" value="10" placeholder="" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col col-xs-12 col-md-4 col-lg-4 col-xl-3">
                                            <div class="form_group">
                                                <div class="qr">
                                                    <img src="images/qr.png" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-md-8 col-lg-8 col-xl-9">
                                            <div class="form_group">
                                                <label class="form_label text-center"><strong>AMOUNT, BTC</strong></label>
                                                <input class="form_control form_control_total text-center" type="text" name="btc" value="0,00134567" placeholder="" disabled>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_label text-center"><strong>BITCOIN ADDRESS</strong></label>
                                                <input class="form_control form_control_total text-center" type="text" name="address" value="37eMhdxQ1kh29kTEAt5krpRbd3qFY2wetS" placeholder="" disabled>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>


                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
