<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Packages</h1>

                        <div class="packages">

                            <div class="package active" data-target=".tab01">
                                <div class="package__wrap">
                                    <div class="package__icon">
                                        <svg class="ico-svg" viewBox="0 0 478.856 478.856" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__money" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <a href="#" class="btn btn_sm">Buy STANDART</a>
                                    <div class="package__name">Standart</div>
                                    <div class="package__price">$50 — $4999</div>
                                    <div class="package__term">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>120 - 180 days</span>
                                    </div>
                                    <ul class="package__data">
                                        <li>
                                            <strong>0,59%</strong>
                                            <span>Profit Day</span>
                                        </li>
                                        <li>
                                            <strong>17,7%</strong>
                                            <span>Month</span>
                                        </li>
                                        <li>
                                            <strong>215,35%</strong>
                                            <span>Year</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="package" data-target=".tab02">
                                <div class="package__wrap">
                                    <div class="package__icon">
                                        <svg class="ico-svg" viewBox="0 0 455.286 455.286" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__banknote" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <a href="#" class="btn btn_sm">Buy MEGA</a>
                                    <div class="package__name">MEGA</div>
                                    <div class="package__price">$5000 — $9999</div>
                                    <div class="package__term">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>120 - 180 days</span>
                                    </div>
                                    <ul class="package__data">
                                        <li>
                                            <strong>0,59%</strong>
                                            <span>Profit Day</span>
                                        </li>
                                        <li>
                                            <strong>17,7%</strong>
                                            <span>Month</span>
                                        </li>
                                        <li>
                                            <strong>215,35%</strong>
                                            <span>Year</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="package" data-target=".tab03">
                                <div class="package__wrap">
                                    <div class="package__icon">
                                        <svg class="ico-svg" viewBox="0 0 59.998 59.998" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__rich" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <a href="#" class="btn btn_sm">Buy VIP</a>
                                    <div class="package__name">VIP</div>
                                    <div class="package__price">$10000 — $20000</div>
                                    <div class="package__term">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>120 - 180 days</span>
                                    </div>
                                    <ul class="package__data">
                                        <li>
                                            <strong>0,59%</strong>
                                            <span>Profit Day</span>
                                        </li>
                                        <li>
                                            <strong>17,7%</strong>
                                            <span>Month</span>
                                        </li>
                                        <li>
                                            <strong>215,35%</strong>
                                            <span>Year</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="source mb_30">

                            <div class="source_tab tab01 active">

                                <div class="source__term">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                    <span>120 - 180 days</span>
                                </div>
                                <div class="source__title">VEXAGLOBAL.STANDART</div>
                                <ul class="source__info">
                                    <li>
                                        <strong>0,59%</strong>
                                        <span>Profit Day</span>
                                    </li>
                                    <li>
                                        <strong>17,7%</strong>
                                        <span>Month</span>
                                    </li>
                                    <li>
                                        <strong>215,35%</strong>
                                        <span>Year</span>
                                    </li>
                                </ul>

                                <div class="source__data">

                                    <div class="source__data_value">$1150</div>
                                    <div class="source__data_info">
                                        <span>Daily profit</span>
                                        <strong>$3.32</strong>
                                    </div>
                                    <div class="source__data_info">
                                        <span>Total return</span>
                                        <strong>$3450.32</strong>
                                    </div>
                                </div>

                                <div class="source__amount">
                                    <div class="source__amount_data">$50</div>
                                    <div class="source__amount_slide">
                                        <input class="source_amount_one" name="source_amount_one" value="" data-type="single" data-min="50" data-max="4999" data-from="1150" data-step="1">
                                    </div>
                                    <div class="source__amount_data">$4999</div>
                                </div>


                            </div>
                            <div class="source_tab tab02">

                                <div class="source__term">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                    <span>120 - 180 days</span>
                                </div>
                                <div class="source__title">VEXAGLOBAL.MEGA</div>
                                <ul class="source__info">
                                    <li>
                                        <strong>0,59%</strong>
                                        <span>Profit Day</span>
                                    </li>
                                    <li>
                                        <strong>17,7%</strong>
                                        <span>Month</span>
                                    </li>
                                    <li>
                                        <strong>215,35%</strong>
                                        <span>Year</span>
                                    </li>
                                </ul>

                                <div class="source__data">

                                    <div class="source__data_value">$1150</div>
                                    <div class="source__data_info">
                                        <span>Daily profit</span>
                                        <strong>$3.32</strong>
                                    </div>
                                    <div class="source__data_info">
                                        <span>Total return</span>
                                        <strong>$3450.32</strong>
                                    </div>
                                </div>

                                <div class="source__amount">
                                    <div class="source__amount_data">$50</div>
                                    <div class="source__amount_slide">
                                        <input class="source_amount_two" name="source_amount_one" value="" data-type="single" data-min="50" data-max="4999" data-from="1150" data-step="1">
                                    </div>
                                    <div class="source__amount_data">$4999</div>
                                </div>


                            </div>
                            <div class="source_tab tab03">

                                <div class="source__term">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__fast" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                    <span>120 - 180 days</span>
                                </div>
                                <div class="source__title">VEXAGLOBAL.VIP</div>
                                <ul class="source__info">
                                    <li>
                                        <strong>0,59%</strong>
                                        <span>Profit Day</span>
                                    </li>
                                    <li>
                                        <strong>17,7%</strong>
                                        <span>Month</span>
                                    </li>
                                    <li>
                                        <strong>215,35%</strong>
                                        <span>Year</span>
                                    </li>
                                </ul>

                                <div class="source__data">

                                    <div class="source__data_value">$1150</div>
                                    <div class="source__data_info">
                                        <span>Daily profit</span>
                                        <strong>$3.32</strong>
                                    </div>
                                    <div class="source__data_info">
                                        <span>Total return</span>
                                        <strong>$3450.32</strong>
                                    </div>
                                </div>

                                <div class="source__amount">
                                    <div class="source__amount_data">$50</div>
                                    <div class="source__amount_slide">
                                        <input class="source_amount_three" name="source_amount_one" value="" data-type="single" data-min="50" data-max="4999" data-from="1150" data-step="1">
                                    </div>
                                    <div class="source__amount_data">$4999</div>
                                </div>


                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">investment</label>
                                    <input type="text" class="form_control" name="name" placeholder="Amount" value="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">wallet</label>
                                    <select class="form_control form_select" name="select">
                                        <option value="Australia">Wallet.USD - 0.00 USD</option>
                                        <option value="Russia">Wallet.USD - 0.00 USD</option>
                                        <option value="China">Wallet.USD - 0.00 USD</option>
                                        <option value="Germany">Wallet.USD - 0.00 USD</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn_xl">CREATE STANDART</button>
                        </div>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
