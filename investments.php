<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>My investments</h1>

                        <div class="tabs">

                            <ul class="investments tabs_nav mb_30">
                                <li class="active">
                                    <a href="#tab01">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 487.222 487.222" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__star" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <strong>STANDART</strong>
                                        <span>15</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab02">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 511.99996 511" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__plane" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <strong>Mega</strong>
                                        <span>15</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab03">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 144.083 144" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__boat" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <strong>VIP</strong>
                                        <span>15</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tabs_item active" id="tab01">

                                <div class="table_responsive">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Investment amount</th>
                                            <th>Created</th>
                                            <th>Balance</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                            <div class="tabs_item" id="tab02">

                                <div class="table_responsive">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Investment amount</th>
                                            <th>Created</th>
                                            <th>Balance</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                            <div class="tabs_item" id="tab03">

                                <div class="table_responsive">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Investment amount</th>
                                            <th>Created</th>
                                            <th>Balance</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#1455096</td>
                                            <td><span class="lead color_blue">$100</span></td>
                                            <td>03.05.2019</td>
                                            <td><span class="lead color_blue">107.55</span></td>
                                            <td>
                                                <a href="#" class="btn btn_border btn_sm">TRANSACTIONS</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                        </div>

                        <ul class="pagination">
                            <li><a href="#"><<span class="hide-xs-only"> Назад</span></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><span class="hide-xs-only">Следующая </span>></a></li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
