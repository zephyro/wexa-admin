<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Partners</h1>

                        <div class="search mb_30">
                            <form class="form">
                                <div class="search__row">
                                    <input type="text" name="search" class="form_control search__input" placeholder="Partner ID">
                                    <button type="submit" class="btn search__btn">SEARCH</button>
                                </div>
                            </form>
                        </div>

                        <div class="table_responsive">
                            <table class="table table_blue">
                                <tr>
                                    <th>Name</th>
                                    <th>Line</th>
                                    <th>Partners</th>
                                    <th>level</th>
                                    <th>INVESTMENT</th>
                                    <th>turnover</th>
                                    <th>REFferal id</th>
                                </tr>
                                <tr class="table_first">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_second">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                                <tr class="table_third">
                                    <td class="color_black"><i class="fas fa-angle-down"></i><span>Vitali21314</span></td>
                                    <td>1</td>
                                    <td>12</td>
                                    <td>3</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td><a class="link_black" href="#">https://vexaglobal.com/r/eqdastgtr</a></td>
                                </tr>
                            </table>
                        </div>


                        <ul class="pagination">
                            <li><a href="#"><<span class="hide-xs-only"> Назад</span></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><span class="hide-xs-only">Следующая </span>></a></li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
