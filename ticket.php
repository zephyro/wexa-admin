<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Tickets</h1>

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>CREATE NEW TICKET</h4>
                                <span class="panel__close"><i class="fas fa-caret-up"></i></span>
                            </div>
                            <div class="panel__body">
                                <form class="form">
                                    <div class="form_group">
                                        <label class="form_label">Title</label>
                                        <input type="text" class="form_control" name="" placeholder="name">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">Message</label>
                                        <textarea class="form_control" name="message" placeholder="" rows="6">Lydnikova 15</textarea>
                                     </div>
                                    <div class="btn_group">
                                        <button type="submit" class="btn btn_lg">Create ticket</button>
                                        <button type="reset" class="btn btn_border">CANCEL</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table_responsive">
                            <table class="table">
                                <tr>
                                    <th>DATA/Time</th>
                                    <th>Topic</th>
                                    <th>status</th>
                                    <th>update</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Close</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_yellow"><i class="far fa-comments"></i><span> Answered</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Close</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_yellow"><i class="far fa-comments"></i><span> Answered</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Close</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_yellow"><i class="far fa-comments"></i><span> Answered</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Close</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><span class="lead color_blue">Create site</span></td>
                                    <td class="color_yellow"><i class="far fa-comments"></i><span> Answered</span></td>
                                    <td>03/05/2019  08:11:25</td>
                                    <td><a href="#" class="btn btn_sm btn_border">view</a></td>
                                </tr>

                            </table>
                        </div>


                        <ul class="pagination">
                            <li><a href="#"><<span class="hide-xs-only"> Назад</span></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><span class="hide-xs-only">Следующая </span>></a></li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
