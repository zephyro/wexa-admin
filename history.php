<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>History</h1>

                        <div class="checkbox_group mb_20">
                            <ul>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Replenishment</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Payments</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Mining</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Trading</span>
                                    </label>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Referral charge</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Translations</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Deposits</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Exchange</span>
                                    </label>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Awards</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span>Getting a crypto wallet</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check">
                                        <span>Deposit cancellation</span>
                                    </label>
                                </li>
                            </ul>
                        </div>

                        <div class="table_responsive">

                            <table class="table">
                                <tr>
                                    <th>date and time</th>
                                    <th>type of transaction</th>
                                    <th>amount</th>
                                    <th>status</th>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_yellow"><i class="fas fa-spinner"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_red"><i class="fas fa-reply"></i><span> Cancel</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_yellow"><i class="fas fa-spinner"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_red"><i class="fas fa-reply"></i><span> Cancel</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_green"><i class="fas fa-check"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_yellow"><i class="fas fa-spinner"></i><span> Credited</span></td>
                                </tr>
                                <tr>
                                    <td>03/05/2019  08:11:25</td>
                                    <td>Trading payment</td>
                                    <td><span class="lead color_blue">$101.77</span></td>
                                    <td class="color_red"><i class="fas fa-arrow-left"></i><span> Cancel</span></td>
                                </tr>

                            </table>
                        </div>

                        <ul class="pagination">
                            <li><a href="#"><<span class="hide-xs-only"> Назад</span></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><span class="hide-xs-only">Следующая </span>></a></li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
