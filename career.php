<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Career</h1>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">8 level</div>
                                <div class="career__heading_name">general director</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">7 level</div>
                                <div class="career__heading_name">junior director</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">6 level</div>
                                <div class="career__heading_name">senior manager</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">5 level</div>
                                <div class="career__heading_name">manager</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">4 level</div>
                                <div class="career__heading_name">senior agent</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">3 level</div>
                                <div class="career__heading_name">Agent</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career career_current">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">2 level</div>
                                <div class="career__heading_name">Beginner</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide" style="width: 25%"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide" style="width: 100%"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                        <div class="career career_success">

                            <div class="career__heading">
                                <div class="career__heading_icon">
                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__briefcase" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="career__heading_level">1 level</div>
                                <div class="career__heading_name">Apprentice</div>
                            </div>
                            <div class="career__body">

                                <ul class="career__step">
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Bonus</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Turnover</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>$0.00</strong>
                                            <span>Investment</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 511.589 511.589" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sale" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>6,00%</strong>
                                            <span>Percent</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="career__step_icon">
                                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__gift" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="career__step_text">
                                            <strong>Iphone X</strong>
                                            <span>Gift</span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="career__progress">
                                    <div class="career__progress_from">1<sup>st</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide" style="width: 100%"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                                <div class="career__progress">
                                    <div class="career__progress_from">2<sup>nd</sup> leg</div>
                                    <div class="career__progress_line">
                                        <div class="career__progress_slide" style="width: 100%"></div>
                                    </div>
                                    <div class="career__progress_to">100 %</div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
