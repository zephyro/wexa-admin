<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Dashboard</h1>

                        <div class="ref mb_30">
                            <div class="ref__title">
                                <span>Partner ID</span>
                                <strong>7412847232123</strong>
                            </div>
                            <div class="ref__link">
                                <div class="ref__link_label">Refferal link:</div>
                                <div class="ref__link_input">
                                    <input class="ref__link_value " name="link" placeholder="" value="https://vexaglobal.com/r/eqdastgtr">
                                </div>
                                <button type="button" class="ref__link_button hide-xs-only hide-sm-only">Copy link</button>
                            </div>
                        </div>

                        <!-- Widget -->
                        <?php include('inc/widget.inc.php') ?>
                        <!-- -->

                        <div class="sticker mb_30">
                            <div class="sticker__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit: <a href="#">Alert link</a></div>
                            <span class="sticker__close"></span>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-lg-7 col-xl-9 col-gutter-lr mb_30">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>PARTNERS</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="l_chart">
                                            <div class="l_chart__legend">
                                                <div class="count count_blue">
                                                    <div class="count__title">Vexa partners</div>
                                                    <div class="count__value"><span>1620</span><i class="fas fa-caret-up color_green"></i></div>
                                                </div>
                                                <div class="count count_yellow">
                                                    <div class="count__title">Direct partners</div>
                                                    <div class="count__value"><span>800</span><i class="fas fa-caret-down color_red"></i></div>
                                                </div>
                                            </div>
                                            <div class="l_chart__content">
                                                <img src="images/chart_01.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-lg-5 col-xl-3 col-gutter-lr mb_30">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>CURRENT LEVEL</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="panel__row">
                                            <div class="panel__col">
                                                <div class="mini_level">
                                                    <div class="mini_level__title"><strong>5</strong><small> level</small></div>
                                                    <div class="mini_level__data">
                                                        <span>Ref. percent</span>
                                                        <strong>13%</strong>
                                                    </div>
                                                    <div class="mini_level__data">
                                                        <span>Next bonus</span>
                                                        <strong>20 000$</strong>
                                                    </div>
                                                    <div class="mini_level__data">
                                                        <span>Need Turnover</span>
                                                        <strong>13 000$</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel__col" style="width: 80px">
                                                <div class="mb_20">
                                                    <img src="images/chart_02.png" class="img-fluid" alt="">
                                                </div>
                                                <div>
                                                    <img src="images/chart_03.png" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <ul class="summary">
                            <li>
                                <span>Self investments</span>
                                <strong>$36782.00</strong>
                            </li>
                            <li>
                                <span>Affiliate income</span>
                                <strong>$543.00</strong>
                            </li>
                            <li>
                                <span>Sales</span>
                                <strong>$12000.00</strong>
                            </li>
                            <li>
                                <span>Sales this month</span>
                                <strong>$3000.00</strong>
                            </li>
                            <li>
                                <span>Something else</span>
                                <strong>$300.00</strong>
                            </li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
