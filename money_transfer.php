<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Money transfer</h1>

                        <!-- Widget -->
                        <?php include('inc/widget.inc.php') ?>
                        <!-- -->

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>transfer between your wallets</h4>
                            </div>
                            <div class="panel__body">
                                <form class="form">

                                    <div class="row">
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">send from</label>
                                                <div class="select_wrap">
                                                    <select class="form_control form_select">
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                    </select>
                                                    <div class="form_icon">
                                                        <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_label">ENTER AMOUNT</label>
                                                <input class="form_control" type="text" name="name" value="10" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">send To</label>
                                                <div class="select_wrap">
                                                    <select class="form_control form_select">
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                    </select>
                                                    <div class="form_icon">
                                                        <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_label text-center"><strong>You'll get, USD</strong></label>
                                                <input class="form_control form_control_total text-center" type="text" name="name" value="31467,32 USD" placeholder="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn_xl">SEND</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>transfer between accounts</h4>
                            </div>
                            <div class="panel__body">
                                <form class="form">

                                    <div class="row">
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">select balance</label>
                                                <div class="select_wrap">
                                                    <select class="form_control form_select">
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                    </select>
                                                    <div class="form_icon">
                                                        <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_label">ENTER AMOUNT</label>
                                                <input class="form_control" type="text" name="name" value="10" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">enter e-mail recipient</label>
                                                <div class="select_wrap">
                                                    <select class="form_control form_select">
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                        <option value="">Main balance</option>
                                                    </select>
                                                    <div class="form_icon">
                                                        <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_label text-center"><strong>You'll get, USD</strong></label>
                                                <input class="form_control form_control_total text-center" type="text" name="name" value="31467,32 USD" placeholder="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn_xl">SEND</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
