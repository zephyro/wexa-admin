<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Settings</h1>

                        <div class="row">

                            <div class="col col-xs-12 col-md-6 col-gutter-lr mb_30">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>personal information</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="form_group">
                                            <label class="form_label">Your name</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="Aleksei">
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label">Your second name</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="Malevin">
                                        </div>
                                        <div class="form_group mb_0">
                                            <label class="form_label">YOUR Nickname</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="lexa">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-xs-12 col-md-6 col-gutter-lr mb_30">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>passport details</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="form_group">
                                            <label class="form_label">Country</label>
                                            <select class="form_control form_select" name="select">
                                                <option value="Australia">Australia</option>
                                                <option value="Russia">Russia</option>
                                                <option value="China">China</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Finland">Finland</option>
                                                <option value="USA">USA</option>
                                                <option value="UK">UK</option>
                                            </select>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label">Date of Birth</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="11.12.1981">
                                        </div>
                                        <div class="form_group mb_0">
                                            <label class="form_label">passport series and number</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="12321421">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="panel mb_30">
                            <div class="panel__heading">
                                <h4>address of registration for obtaining a contract</h4>
                            </div>
                            <div class="panel__body">

                                <div class="row">

                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">region</label>
                                            <select class="form_control form_select" name="select">
                                                <option value="Australia">Australia</option>
                                                <option value="Russia">Russia</option>
                                                <option value="China">China</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Finland">Finland</option>
                                                <option value="USA">USA</option>
                                                <option value="UK">UK</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">City</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="Moscow">
                                        </div>
                                    </div>

                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">address</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="Lydnikova 15">
                                        </div>
                                    </div>

                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">postcode</label>
                                            <input type="text" class="form_control" name="name" placeholder="" value="1234567">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="accept mb_30">
                            <button type="submit" class="btn btn_xl">save changes</button>
                        </div>


                        <div class="row">

                            <div class="col col-xs-12 col-md-6 col-gutter-lr mb_30">

                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>cryptocurrency</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="form_group">
                                            <div class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </div>
                                            <input type="text" name="btc" class="form_control form_control_total" value="37eMhdxQ1kh29kTEAt5krpRbd3qFY2wetS" placeholder="">
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn btn_icon input_group__btn">
                                                    <b>
                                                        <svg class="ico-svg" viewBox="0 0 438.533 438.533" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn btn_icon input_group__btn">
                                                    <b>
                                                        <svg class="ico-svg" viewBox="0 0 438.533 438.533" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn btn_icon input_group__btn">
                                                    <b>
                                                        <svg class="ico-svg" viewBox="0 0 438.533 438.533" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn btn_icon input_group__btn">
                                                    <b>
                                                        <svg class="ico-svg" viewBox="0 0 438.533 438.533" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form_group mb_0">
                                            <label class="form_label form_label_icon">
                                                <i><img src="img/icon__bitcoin.png" class="img-fluid" alt="icon"></i>
                                                <span>Bitcoin</span>
                                            </label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn btn_icon input_group__btn">
                                                    <b>
                                                        <svg class="ico-svg" viewBox="0 0 438.533 438.533" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col col-xs-12 col-md-6 col-gutter-lr mb_30">

                                <div class="panel mb_30">
                                    <div class="panel__heading">
                                        <h4>password change form</h4>
                                    </div>
                                    <div class="panel__body">

                                        <div class="row">
                                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                                <div class="form_group">
                                                    <label class="form_label">New password</label>
                                                    <input type="password" class="form_control" name="name" placeholder="********" value="">
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                                <div class="form_group">
                                                    <label class="form_label">REPEAT new password</label>
                                                    <input type="password" class="form_control" name="name" placeholder="********" value="">
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                                <div class="form_group">
                                                    <label class="form_label">YOur pin</label>
                                                    <input type="password" class="form_control" name="name" placeholder="********" value="">
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                                <div class="form_group">
                                                    <label class="form_label"></label>
                                                    <button type="submit" class="btn btn_xxl">change password</button>
                                                </div>
                                            </div>
                                            <div class="col col-xs-12 col-gutter-lr">
                                                <div class="form_group">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check">
                                                        <span>Turn on Google Authenticator</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel__heading">
                                        <h4>Contact details</h4>
                                    </div>
                                    <div class="panel__body">
                                        <div class="form_group">
                                            <label class="form_label">Phone</label>
                                            <div class="input_group">
                                                <input type="text" name="name" class="form_control input_group__form" placeholder="">
                                                <button type="submit" class="btn input_group__btn">confirm</button>
                                            </div>
                                        </div>
                                        <div class="form_group">
                                            <label class="form_label">e-mail</label>
                                            <div class="form_inline">
                                                <input type="text" name="search" class="form_inline__form" placeholder="">
                                                <div class="form_inline__text color_green"><i class="fas fa-check"></i> <span>confirmed</span></div>
                                            </div>
                                        </div>
                                        <div class="form_group mb_0">
                                            <label class="form_checkbox">
                                                <input type="checkbox" name="check" checked>
                                                <span>Turn on 2fa</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
