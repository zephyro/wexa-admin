<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <section class="main">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="content">

                    <div class="container">

                        <!-- Markets -->
                        <?php include('inc/markets.inc.php') ?>
                        <!-- -->

                        <h1>Marketing</h1>

                        <ul class="accordion">
                            <li>
                                <div class="accordion__heading">
                                    <h3>Binary plan presentation</h3>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div class="accordion__content">
                                    <div class="double_list">
                                        <dl class="double_list_heading">
                                            <dt>PRESENTATION</dt>
                                            <dd>FILES</dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </li>
                            <li class="open">
                                <div class="accordion__heading">
                                    <h3>VEXA GLOBAL presentation</h3>
                                    <i class="fas fa-angle-down"></i>
                                </div>


                                <div class="accordion__content" style="display: block">

                                    <div class="double_list">
                                        <dl class="double_list_heading">
                                            <dt>PRESENTATION</dt>
                                            <dd>FILES</dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                    </div>

                                </div>
                            </li>
                            <li>
                                <div class="accordion__heading">
                                    <h3>REWARDS</h3>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div class="accordion__content">
                                    <div class="double_list">
                                        <dl class="double_list_heading">
                                            <dt>PRESENTATION</dt>
                                            <dd>FILES</dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__heading">
                                    <h3>BANNERS</h3>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div class="accordion__content">
                                    <div class="elem_row">

                                        <div class="elem_col">
                                            <div class="elem">
                                                <div class="elem__image">
                                                    <div class="elem__image_wrap">
                                                        <div class="elem__image_item">
                                                            <img src="img/elem.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elem__content">
                                                    <div class="elem__size">160x600</div>
                                                    <ul class="elem__btn">
                                                        <li><a href="#" class="btn btn_sm">REFLINK TOP</a></li>
                                                        <li><a href="#" class="btn btn_border btn_sm">REFLINK bOTtoM</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="elem_col">
                                            <div class="elem">
                                                <div class="elem__image">
                                                    <div class="elem__image_wrap">
                                                        <div class="elem__image_item">
                                                            <img src="img/elem.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elem__content">
                                                    <div class="elem__size">160x600</div>
                                                    <ul class="elem__btn">
                                                        <li><a href="#" class="btn btn_sm">REFLINK TOP</a></li>
                                                        <li><a href="#" class="btn btn_border btn_sm">REFLINK bOTtoM</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="elem_col">
                                            <div class="elem">
                                                <div class="elem__image">
                                                    <div class="elem__image_wrap">
                                                        <div class="elem__image_item">
                                                            <img src="img/elem.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elem__content">
                                                    <div class="elem__size">160x600</div>
                                                    <ul class="elem__btn">
                                                        <li><a href="#" class="btn btn_sm">REFLINK TOP</a></li>
                                                        <li><a href="#" class="btn btn_border btn_sm">REFLINK bOTtoM</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="elem_col">
                                            <div class="elem">
                                                <div class="elem__image">
                                                    <div class="elem__image_wrap">
                                                        <div class="elem__image_item">
                                                            <img src="img/elem.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elem__content">
                                                    <div class="elem__size">160x600</div>
                                                    <ul class="elem__btn">
                                                        <li><a href="#" class="btn btn_sm">REFLINK TOP</a></li>
                                                        <li><a href="#" class="btn btn_border btn_sm">REFLINK bOTtoM</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="elem_col">
                                            <div class="elem">
                                                <div class="elem__image">
                                                    <div class="elem__image_wrap">
                                                        <div class="elem__image_item">
                                                            <img src="img/elem.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elem__content">
                                                    <div class="elem__size">160x600</div>
                                                    <ul class="elem__btn">
                                                        <li><a href="#" class="btn btn_sm">REFLINK TOP</a></li>
                                                        <li><a href="#" class="btn btn_border btn_sm">REFLINK bOTtoM</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__heading">
                                    <h3>Printables</h3>
                                    <i class="fas fa-angle-down"></i>
                                </div>
                                <div class="accordion__content">
                                    <div class="double_list">
                                        <dl class="double_list_heading">
                                            <dt>PRESENTATION</dt>
                                            <dd>FILES</dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt><span class="text-uppercase color_blue">EXPASSET</span> - Vexa Global Presentation EN</dt>
                                            <dd>
                                                <a href="#" class="btn btn_sm btn_border btn_pdf">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 439.875 439.875" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>DOWNLOAD</span>
                                                </a>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>

            </section>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
