<div class="row">

    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-gutter-lr mb_30">
        <div class="widget">
            <div class="widget__icon">
                <svg class="ico-svg" viewBox="0 0 401.601 401.6" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__coin_stack" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>
            <div class="widget__heading">
                MAIN<br/>
                BALANCE
            </div>
            <div class="widget__data">$36782.00</div>
            <div class="widget__info">
                <span class="widget__info_value widget__info_green">+11%</span>
                <div class="widget__info_text">From previous period</div>
            </div>
        </div>
    </div>

    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-gutter-lr mb_30">
        <div class="widget">
            <div class="widget__icon">
                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__earnings" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>
            <div class="widget__heading">
                INVESTMENT<br/>
                BALANCE
            </div>
            <div class="widget__data">$5782.32</div>
            <div class="widget__info">
                <span class="widget__info_value widget__info_red">-11%</span>
                <div class="widget__info_text">From previous period</div>
            </div>
        </div>
    </div>

    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-gutter-lr mb_30">
        <div class="widget">
            <div class="widget__icon">
                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__affiliate" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>
            <div class="widget__heading">
                Affiliate<br/>
                balance
            </div>
            <div class="widget__data">$10.00</div>
            <div class="widget__info">
                <span class="widget__info_value widget__info_green">+11%</span>
                <div class="widget__info_text">From previous period</div>
            </div>
        </div>
    </div>

    <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-3 col-gutter-lr mb_30">
        <div class="widget">
            <div class="widget__wrap">
                <div class="widget__item">
                    <i class="widget__item_icon">
                        <svg class="ico-svg" viewBox="0 0 47 47" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__economic_investment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <div class="widget__item_title">INTERESTING BALANCE</div>
                    <div class="widget__item_value">$560</div>
                </div>
                <div class="widget__item">
                    <i class="widget__item_icon">
                        <svg class="ico-svg" viewBox="0 0 47 47" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__economic_investment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <div class="widget__item_title">NEW BALANCE</div>
                    <div class="widget__item_value">$560</div>
                </div>
                <div class="widget__item">
                    <i class="widget__item_icon">
                        <svg class="ico-svg" viewBox="0 0 47 47" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__economic_investment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <div class="widget__item_title">SOMETHING BALANCE</div>
                    <div class="widget__item_value">$560</div>
                </div>
            </div>
        </div>
    </div>
</div>