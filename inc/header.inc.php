<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__toggle sidebar_toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>

            <ul class="header__content">

                <li>
                    <div class="notify">
                        <div class="notify__label">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 510 510" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__notifications" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>123</span>
                        </div>
                        <div class="notify__dropdown">
                            <ul>
                                <li>
                                    <a href="#">
                                        <strong>Your order is placed</strong>
                                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <strong>Your order is placed</strong>
                                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit sure it</span>
                                    </a>
                                </li>
                            </ul>
                            <a class="notify__view" href="#">View all</a>
                        </div>
                    </div>
                </li>

                <li>
                    <a class="header__auth" href="#">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 471.2 471.2" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__logout" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Logout</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</header>