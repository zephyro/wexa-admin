<aside class="sidebar">

    <a href="/" class="sidebar__logo">
        <i>
            <img src="img/logo.svg" class="img-fluid" alt="">
        </i>
    </a>

    <div class="sidebar__wrap">

        <nav class="sidenav">
            <div class="sidenav__heading"><span>Main</span></div>
            <ul>
                <li class="active">
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 481 481" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 481 481" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Settings</span>
                    </a>
                </li>
            </ul>
        </nav>

        <nav class="sidenav mb_40">
            <div class="sidenav__heading"><span>Components</span></div>
            <ul>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 54.953 54.953" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Packages</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>My investments</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 334.877 334.877" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Refill</span>
                        <i class="fas fa-angle-right"></i>
                    </a>
                    <ul>
                        <li><a href="#">Second Nav 01</a></li>
                        <li><a href="#">Second Nav 02</a></li>
                        <li><a href="#">Second Nav 03</a></li>
                        <li><a href="#">Second Nav 04</a></li>
                        <li><a href="#">Second Nav 05</a></li>
                        <li><a href="#">Second Nav 06</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 442.003 442.003" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Payouts</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 490.1 490.1" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Money transfer</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_08" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>History</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 424 424" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_09" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Partners</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512.005 512.005" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_10" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Tickets</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 372 372" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_11" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Career</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <b>
                            <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_12" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </b>
                        <span>Marketing</span>
                    </a>
                </li>
            </ul>
        </nav>

    </div>

</aside>