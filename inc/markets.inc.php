<div class="markets mb_20">
    <div class="markets__slider swiper-container">
        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">GG</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ITUB</div>
                            <div class="markets__top_elem color_red"><span>-9,75%</span><i class="fas fa-caret-down"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ECA</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">GG</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ITUB</div>
                            <div class="markets__top_elem color_red"><span>-9,75%</span><i class="fas fa-caret-down"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ECA</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">GG</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ITUB</div>
                            <div class="markets__top_elem color_red"><span>-9,75%</span><i class="fas fa-caret-down"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="markets__item">
                    <div class="markets__row">
                        <div class="markets__top">
                            <div class="markets__top_elem">ECA</div>
                            <div class="markets__top_elem color_green"><span>+12,75%</span><i class="fas fa-caret-up"></i></div>
                        </div>
                        <div class="markets__bottom">
                            <div class="markets__bottom_elem">$10.00</div>
                            <div class="markets__bottom_elem">$12346765</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>